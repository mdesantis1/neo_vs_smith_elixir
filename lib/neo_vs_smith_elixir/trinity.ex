defmodule NeoVsSmithElixir.Trinity do
  @behaviour :gen_server

  def start_link(matrix_pid) do
    :gen_server.start_link({:local, :trinity}, __MODULE__, [matrix_pid], [])
  end

  def init([matrix_pid]) do
    IO.puts("Trinity: ready")
    {:ok, {matrix_pid}}
  end

  def handle_call(:eval, _from, state) do
    {:reply, state}
  end

  defp think(message) do
    IO.puts("Trinity thinks: #{message}")
  end

  def handle_cast({:say, message, _}, state) do
    IO.puts("Trinity says: #{message}")
    think("God, I hope I haven't been too naughty last night. We're even not yet married")
    {:noreply, state}
  end

  def handle_cast(_message, state) do
    {:noreply, state}
  end

  def handle_info(_info, state) do
    {:noreply, state}
  end

  def terminate(_reason, _state) do
    :ok
  end

  def code_change(_old_vsn, state, _extra) do
    {:ok, state}
  end

end