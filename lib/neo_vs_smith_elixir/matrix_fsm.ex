defmodule NeoVsSmithElixir.MatrixFSM do
  @behaviour :gen_fsm

  def start_link(matrix_pid) do
    :gen_fsm.start_link({:local, __MODULE__}, __MODULE__, [matrix_pid], [])
  end

  def init(matrix_pid) do
    {:ok, screenplay} = :application.get_env(:screenplay)
    :gen_fsm.send_event_after(10000, :step)
    {:ok, :step, {matrix_pid, screenplay}}
  end

  # states
  def step(_, {_, []}) do
    {:stop, "Follow me on GitHub, if you like: ProGNOMmers", :ok}
  end

  def step(_, {matrix_pid, [{role, {action, payload, {:time, time}}}|screenplay]}) do
    case action do
      :fight -> :gen_server.cast(:erlang.whereis(:smith), {action, payload, time})
      _      -> :ok
    end
    :gen_server.cast(:erlang.whereis(role), {action, payload, time})
    :gen_fsm.send_event_after(time * 1000, :step)
    {:next_state, :step, {matrix_pid, screenplay}}
  end

  # callbacks (dummies)
  def handle_event(_, _, state) do
    {:noreply, state}
  end

  def handle_sync_event(_, _, _, state) do
    {:reply, state}
  end

  def handle_info(_, _, state) do
    {:reply, state}
  end

  def terminate(_, _, _) do
    :ok
  end

  def code_change(_, _, _, state) do
    {:ok, state}
  end
end
