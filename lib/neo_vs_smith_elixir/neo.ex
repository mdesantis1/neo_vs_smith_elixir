defmodule NeoVsSmithElixir.Neo do
  @behaviour :gen_server

  def start_link(matrix_pid) do
    :gen_server.start_link({:local, :neo}, __MODULE__, [matrix_pid], [])
  end

  def init([matrix_pid]) do
    IO.puts("Neo: ready")
    {ok, frequency} = :application.get_env(:beating_frequency)
    {ok, {matrix_pid, frequency}}
  end

  def handle_call(:eval, _from, state) do
    {:reply, state}
  end

  defp think(msg) do
    IO.puts("Neo thinks: #{msg}")
  end

  def handle_cast({:resist, _, _}, state) do
    IO.puts("Neo resists Smith's assimilation attempt")
    think("F... you!")
    {:noreply, state}
  end

  def handle_cast({:clean, :pole, _}, state) do
    IO.puts("Neo cleans the pole with the body of a Smith clone")
    think("Never liked your suit, dude")
    {:noreply, state}
  end

  def handle_cast({:lose, :pole, _}, state) do
    IO.puts("Neo loses the pole")
    think("F...!")
    {:noreply, state}
  end

  def handle_cast({:play, :pole, _}, state) do
    IO.puts("Neo plays friendly with the pole")
    think("It's better than this stupid spoon")
    {:noreply, state}
  end

  def handle_cast({:look, :realistic, _}, state) do
    IO.puts("Neo looks around, pure realism in his eyes")
    think("time to get the hell out of here, before I get hurt")
    {:noreply, state}
  end

  def handle_cast({:bend, :matrix, _}, state) do
    IO.puts("Neo quickly bends the matrix under his feet")
    think("Careful, man. Don't forget what happened in the restroom")
    {:noreply, state}
  end

  def handle_cast({:cast_off, :smith, _}, state) do
    IO.puts("Neo casts off a dozen of Smith clones")
    think("Hey, take me out for dinner before you get close like that")
    {:noreply, state}
  end

  def handle_cast({:fly, where, _}, state) do
    msg = case where do
            :reposition ->
              think("I believe I can fly, I believe I can touch the sky")
              "up to have a better fighting position"
            :escape     ->
              think("Make some tea, Trinity")
              "away, being very realistic"
          end
    IO.puts("Neo flies #{msg}")
    {:noreply, state}
  end

  def handle_cast({:grab, what, _}, state) do
    msg = case what do
            :pole  ->
              think("Oh, look, a pole")
              "a pole"
            :smith ->
              think("You should go to a gym, dude")
              "a Smith clone"
          end
    IO.puts("Neo grabs #{msg}")
    {:noreply, state}
  end

  def handle_cast({:crash, what, _}, state) do
    msg = case what do
            :bench ->
              think("Man, exactly like the other day after the pub")
              "over a bench"
            :wall  ->
              think("Really, like the other day after the pub")
              "into a wall"
          end
    IO.puts("Neo crashes #{msg}")
    {:noreply, state}
  end

  def handle_cast({:fight, weapon, time}, state) do
    msg = case weapon do
            :bare  -> "his own body"
            :pole  -> "a pole"
            :smith -> "a Smith clone"
          end
    IO.puts("Neo fights using #{msg} as weapon")
    think("Trinity, you've been a naughty girl last night. I liked it.")
    {_, frequency} = state
    smiths = :supervisor.which_children(Smith.Supervisor)
    :random.seed()
    :gen_server.cast(self(), {:beat, frequency, weapon, smiths, time})
    {:noreply, state}
  end

  def handle_cast({:beat, _frequency, _weapon, _smiths, 0}, state) do
    {:noreply, state}
  end

  def handle_cast({:beat, frequency, weapon, smiths, time}, state) do
    time_delta = time - frequency
    sleep = case time_delta < 0 do
              true -> time
              _    -> frequency
            end

    :timer.sleep(sleep * 1000)
    weapon_factor = case weapon do
                      :bare  -> 1
                      :pole  -> 4
                      :smith -> 2
                    end
    smiths_length = length(smiths)
    clone_factor = case smiths_length < 10 do
                     true -> 1
                     _    -> case smiths_length < 100 do
                       true -> 2
                       _    -> case smiths_length < 1000 do
                         true -> 4
                         _    -> 8
                       end
                     end
                   end
    max = round(((smiths_length / frequency) * weapon_factor) / clone_factor)
    rest_smiths = case max > 0 do
                    true ->
                      to_kill = :random.uniform(max)
                      IO.puts("Neo eliminates #{integer_to_list(to_kill)} of #{integer_to_list(smiths_length+1)}")
                      think("Aha, another bunch of suckers gone!")
                      beat(smiths, to_kill)
                    _ ->
                      think("It's #{integer_to_list(Smith.Supervisor.current()+1)} of them, damn it!")
                      smiths
                  end
    :gen_server.cast(self(), {:beat, frequency, weapon, rest_smiths, time - sleep})
    {:noreply, state}
  end

  def handle_cast(_, state) do
    {:noreply, state}
  end

  defp beat(smiths, 0) do
    smiths
  end
  
  defp beat([{_, pid, _, _}|l], to_kill) do
    :erlang.exit(pid, :kill)
    beat(l, to_kill-1)
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  def terminate(_, _) do
    :ok
  end

  def code_change(_, state, _) do
    {:ok, state}
  end

end