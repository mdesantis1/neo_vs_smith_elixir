defmodule NeoVsSmithElixir.Smith do
  @behaviour :gen_server

  def start_link(matrix_pid) do
    :gen_server.start_link({:local, :smith}, __MODULE__, [matrix_pid], [])
  end

  def init([matrix_pid]) do
    IO.puts("Smith: ready")
    {:ok, frequency} = :application.get_env(:beating_frequency)
    {:ok, max_attacking} = :application.get_env(:max_smiths_attacking)
    {:ok, {matrix_pid, frequency, max_attacking}}
  end

  def handle_call(:eval, _from, state) do
    {:reply, state}
  end

  defp think(message) do
    IO.puts("Smith thinks: #{message}")
  end

  def handle_cast({:clone, n, _}, state) do
    IO.puts("Smith creates #{integer_to_list(n)} clones of himself")
    think("for (int i = 1; i < #{integer_to_list(n)}; i++) {")
    :smith_sup.clone(n)
    {:noreply, state}
  end

  def handle_cast({:say, message, _}, state) do
    IO.puts("Smith says: #{message}")
    think("System.out.println(\"#{message}\");")
    {:noreply, state}
  end

  def handle_cast({:assimilate, whom, _}, state) do
    message = case whom do
                :neo     -> "neo"
                :jackson -> "Jackson"
              end
    IO.puts("Smith sticks his hand into #{message}'s body to assimilate him")
    think("try { assimilate(neo); } catch (Exception e) {")
    {:noreply, state}
  end

  def handle_cast({:look, how, _}, state) do
    message = atom_to_list(how)
    IO.puts("Smith looks #{message}")
    think("throw new LookingException(\"#{message}\");")
    {:noreply, state}
  end

  def handle_cast({:squish, :fruits, _}, state) do
    IO.puts("Smith squishes some fruits on the ground")
    think("if (fruitsAvailable) { squish(fruitsCollection); }")
    {:noreply, state}
  end

  def handle_cast({:crash, :bench, _}, state) do
    IO.puts("Smith crashes over a bench")
    think("if (benchAvailable) { crash(bench); }")
    {:noreply, state}
  end

  def handle_cast({:pile_up, :neo, _}, state) do
    IO.puts("Smith piles up dozens of his clones on neo")
    think("for (int i = 0; i < 24; i++) { pileUp(neo); }")
    {:noreply, state}
  end

  def handle_cast({:fight, _, time}, state) do
    {_, frequency, max_attacking} = state
    :random.seed()
    :gen_server.cast(self(), {:beat, frequency, max_attacking, time})
    {:noreply, state}
  end

  def handle_cast({:beat, _frequency, _max_attacking, 0}, state) do
    {:noreply, state}
  end

  def handle_cast({:beat, frequency, max_attacking, time}, state) do
    time_delta = time - frequency
    sleep = case time_delta < 0 do
              true -> time
              _    -> frequency
            end
    :timer.sleep(sleep * 1000)
    smiths = :supervisor.which_children(__MODULE__.Supervisor)
    max = :random.uniform(max_attacking)
    beat(smiths, max)
    :gen_server.cast(self(), {:beat, frequency, max_attacking, time - sleep})
    {:noreply, state}
  end

  def handle_cast(_, state) do
    {:noreply, state}
  end

  defp beat([], _) do
    :ok
  end

  defp beat(_, 0) do
    :ok
  end

  defp beat([{_, pid, _, _}|smiths], n) do
    case :erlang.is_process_alive(pid) do
      true ->
        :gen_server.cast(pid, {:beat})
        beat(smiths, n - 1)
      _ ->
        beat(smiths, n)
    end
  end

  def handle_info(_info, state) do
    {:noreply, state}
  end

  def terminate(_reason, _state) do
    :ok
  end

  def code_change(_old_vsn, state, _extra) do
    {:ok, state}
  end

end