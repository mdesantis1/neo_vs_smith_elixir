defmodule NeoVsSmithElixir.Supervisor do
  @behaviour :supervisor

  # API
  def start_link() do
    IO.puts("Matrix: ready")
    :supervisor.start_link({:local, __MODULE__}, __MODULE__, [])
  end

  # Callbacks
  def init([]) do
    fsm       = child MatrixFSM,        :worker
    neo       = child Neo,              :worker
    smith_sup = child Smith.Supervisor, :supervisor
    smith     = child Smith,            :supervisor
    woman     = child Woman,            :worker
    jackson   = child Jackson,          :worker
    trinity   = child Trinity,          :worker
    {:ok, {{:one_for_one, 5, 10}, [fsm, neo, smith_sup, smith, woman, jackson, trinity]}}
  end

  # Utils

  # Helper for declaring children of supervisor
  defp child(i, type) do
    # IO.inspect {i, {i, :start_link, [self()]}, :temporary, 5000, type, [i]}
    {i, {i, :start_link, [self()]}, :temporary, 5000, type, [i]}
  end

end