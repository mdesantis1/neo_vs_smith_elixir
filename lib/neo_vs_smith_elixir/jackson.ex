defmodule NeoVsSmithElixir.Jackson do
  @behaviour :gen_server

  def start_link(matrix_pid) do
    :gen_server.start_link({:local, :jackson}, __MODULE__, [matrix_pid], [])
  end

  def init([matrix_pid]) do
    IO.puts("Jackson: ready")
    {:ok, {matrix_pid}}
  end

  def handle_call(:eval, _from, state) do
    {:reply, state}
  end

  defp think(message) do
    IO.puts("Jackson thinks: #{message}")
  end

  def handle_cast({:assimilate, :woman, _}, state) do
    IO.puts("Jackson assimilates Woman, terribly deforming her face")
    think("begin Assimilate(woman) end.")
    {:noreply, state}
  end

  def handle_cast({:say, message, _}, state) do
    IO.puts("Jackson says: #{message}")
    think("begin WriteLn('#{message}') end.")
    {:noreply, state}
  end

  def handle_cast(_, state) do
    {:noreply, state}
  end

  def handle_info(_info, state) do
    {:noreply, state}
  end

  def terminate(_reason, _state) do
    :ok
  end

  def code_change(_old_vsn, state, _extra) do
    {:ok, state}
  end

end