defmodule NeoVsSmithElixir.Woman do
  @behaviour :gen_server

  def start_link(matrix_pid) do
    :gen_server.start_link({:local, :woman}, __MODULE__, [matrix_pid], [])
  end

  def init([matrix_pid]) do
    IO.puts("Woman: ready")
    {:ok, {matrix_pid}}
  end

  def handle_call(:eval, _from, state) do
    {:reply, state}
  end

  defp think(message) do
    IO.puts("Woman thinks: #{message}")
  end

  def handle_cast({:appear, _, _}, state) do
    IO.puts("Woman with food bags appears")
    think("What the..?")
    {:noreply, state}
  end

  def handle_cast({:drop, :bags, _}, state) do
    IO.puts("Woman drops her bags on the ground, scared like hell")
    think("Oops, I did it again")
    {:noreply, state}
  end

  def handle_cast(_, state) do
    {:noreply, state}
  end

  def handle_info(_info, state) do
    {:noreply, state}
  end

  def terminate(_reason, _state) do
    :ok
  end

  def code_change(_old_vsn, state, _extra) do
    {:ok, state}
  end

end