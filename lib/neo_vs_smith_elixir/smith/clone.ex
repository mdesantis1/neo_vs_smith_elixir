defmodule NeoVsSmithElixir.Smith.Clone do
  @behaviour :gen_server

  def start_link() do
    :gen_server.start_link(__MODULE__, [], [])
  end

  def init(_) do
    {:ok, :ok}
  end

  def handle_call(:eval, _from, state) do
    {:reply, state}
  end

  def handle_cast({:beat}, state) do
    IO.puts("Smith's clone #{pid_to_list(self())}: attacking Neo")
    {:noreply, state}
  end

  def handle_cast(_msg, state) do
    {:noreply, state}
  end

  def handle_info(_info, state) do
    {:noreply, state}
  end

  def terminate(_reason, _state) do
    :ok
  end

  def code_change(_old_vsn, state, _extra) do
    {:ok, state}
  end
end