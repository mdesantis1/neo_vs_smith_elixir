defmodule NeoVsSmithElixir.Smith.Supervisor do
  @behaviour :supervisor

  # API
  def start_link(_matrix_pid) do
    :supervisor.start_link({:local, __MODULE__}, __MODULE__, [])
  end

  def current() do
    length(:supervisor.which_children(__MODULE__))
  end

  def clone(n) do
    length = current()
    to_start = case length do
                 0 -> n
                 _ -> n-length
               end
    clone_int(to_start)
  end

  # callbacks
  def init([]) do
    {:ok, {{:simple_one_for_one, 1000, 1}, [spec()]}}
  end

  # other
  defp spec() do
    child(Smith.Clone, :worker)
  end

  defp clone_int(0) do
    :ok
  end
  
  defp clone_int(n) do
    :supervisor.start_child(__MODULE__, [])
    clone_int(n-1)
  end

  defp child(i, type) do
    {i, {i, :start_link, []}, :permanent, 5000, type, [i]}
  end
end