defmodule NeoVsSmithElixir do

  @behaviour :application

  def start() do
    start(:ok, :ok)
  end

  def start(_start_type, _start_args) do
    IO.puts("Neo vs. Smiths, the Erlang version.")
    __MODULE__.Supervisor.start_link()
  end

  def stop(_state) do
    :ok
  end

end
