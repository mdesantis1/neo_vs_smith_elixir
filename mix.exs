defmodule NeoVsSmithElixir.Mixfile do
  use Mix.Project

  def project do
    [ app: :neo_vs_smith_elixir,
      version: "0.0.1",
      erlc_opts: [:debug_info, :warnings_as_errors],
      deps: deps ]
  end

  # Configuration for the OTP application
  def application do
    [env: [ciao: :ciao]]
  end

  # Returns the list of dependencies in the format:
  # { :foobar, "0.1", git: "https://github.com/elixir-lang/foobar.git" }
  defp deps do
    []
  end
end
